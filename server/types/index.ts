/* eslint-disable @typescript-eslint/no-explicit-any */
export type MessageProps = {
  messageId?: string;
  messageType?: string;
  textOrPathToFile?: string;
  roomId?: string | any;
  userName?: string;
  createdAt?: number;
}

export type OnErrorProps = {
  err?: any;
  req?: any;
  res?: any;
  next?: any;
}