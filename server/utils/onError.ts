import { OnErrorProps } from "../types";

export const onError = ({ err, res }: OnErrorProps) => {
  console.log('Error', err);

  if (res) {
    const status = err.status || err.statusCode || 500;
    const message = err.message || "Something went wrong. Try again later";
    res.status(status).json({ message });
  }
}