import { existsSync, mkdirSync } from "fs";
import multer from 'multer';
import { dirname, join } from "path";
import { fileURLToPath } from "url";
import uuid from 'uuid';

const _dirname = dirname(fileURLToPath(import.meta.url));

export const upload = multer({
  storage: multer.diskStorage({
    destination: async (req, _, cb) => {
      const roomId: any = req.headers['x-room-id'];
      const dirPath = join(_dirname, '../files', roomId);

      if (!existsSync(dirPath)) {
        mkdirSync(dirPath, { recursive: true })
      }

      cb(null, dirPath);
    },
    filename: (_, file, cb) => {
      const fileName = `${uuid.v4()} - ${file.originalname}}`;
      
      cb(null, fileName);
    }
  })
});