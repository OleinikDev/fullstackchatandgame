import express from 'express';
import cors, { CorsOptions } from 'cors';
import { createServer } from 'http';
import mongoose, { ConnectOptions } from 'mongoose';
import { Server } from 'socket.io';
import { onConnection } from './helpers/onConnection';
import { getFilePath } from './utils/file';
import { onError } from './utils/onError';
import { upload } from './utils/upload';
import { MONGODB_CONNECT } from './constants';

const app = express();
const PORT = process.env.PORT;
const server = createServer(app);
const io = new Server(server, {
  cors: process.env.ALLOWED_ORIGIN as CorsOptions,
  serveClient: false
})

app.use(cors({
  origin: process.env.ALLOWED_ORIGIN
}));

app.use(express.json());


app.use('/upload', upload.single('file'), (req, res) => {
  if (!req.file) return res.sendStatus(400);

  const relativeFilePath = req.file.path.replace(/\\/g, '/').split('server/files')[1];

  res.status(200).json(relativeFilePath);
});

app.use('/files', (req, res) => {
  const filePath = getFilePath(req.url);

  res.status(200).sendFile(filePath);
});

app.use(onError);

try {
  mongoose.connect(MONGODB_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  } as ConnectOptions);

  console.log('🚀 Connected');
} catch (err) {
  console.log('Error: ', err);
}

io.on('connection', (socket) => {
  onConnection(io, socket);
})

server.listen(PORT, () => {
  console.log(`🚀 Server started on port ${PORT}`)
})

// socketIO.on('conenction', (socket: { id: any; on: (arg0: string, arg1: () => void) => void; }) => {
//   console.log(`⚡: ${socket.id} user just connected!`);
//   socket.on('disconnect', () => {
//     console.log('🔥: A user disconnected');
//   });
// });

// http.listen(PORT, () => {
//   console.log(`Server started on port ${PORT}`);
// });
