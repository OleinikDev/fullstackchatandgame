/* eslint-disable @typescript-eslint/no-explicit-any */
import Message from '../models/messages.model';
import { MessageProps } from '../types';
import { removeFile } from '../utils/file';
import { onError } from '../utils/onError';

const messages: MessageProps = {
  roomId: "",
  messageId: '',
  messageType: '',
  textOrPathToFile: '',
  userName: '',
  createdAt: 0
};

export const messageHandlers = (io: any, socket: any) => {
  const { roomId } = socket;

  const updateMessageList = () => {
    io.to(roomId).emit('message_list:update', messages[roomId as keyof MessageProps]);
  };

  socket.on('message:get', async () => {
    try {
      const _messages = await Message.find({ roomId });
      messages[roomId as keyof MessageProps] = _messages;

      updateMessageList();
    } catch (err) {
      console.log('Error: ', err);
    }
  });

  socket.on('message:add', (message: MessageProps) => {
    Message.create(message).catch(onError);

    message.createdAt = Date.now();

    messages[roomId as keyof MessageProps].push(message);

    updateMessageList();
  });

  socket.on('message:remove', (message: MessageProps) => {
    const { messageId, messageType, textOrPathToFile } = message;

    Message.deleteOne({ messageId })
      .then(() => {
        if (messageType !== "text") {
          removeFile(textOrPathToFile as string);
        }
      })
      .catch(onError);
  
    
    message[roomId as keyof MessageProps] = message[roomId as keyof MessageProps].filter((msg: any) => msg.messageId !== messageId);

    updateMessageList();
  });
}