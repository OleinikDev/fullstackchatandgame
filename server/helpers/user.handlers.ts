/* eslint-disable @typescript-eslint/no-explicit-any */
const users: any = {};

export const userHandlers = (io: any, socket: any) => {
  const { roomId, userName } = socket.handshake.query;

  if (!users[roomId]) {
    users[roomId] = [];
  }

  const updateUsersList = () => {
    io.to(roomId).emit('user_list:update', users[roomId]);
  }

  socket.on('user:add', async (user: any) => {
    socket.to(roomId).emit('log', `User ${userName} connected`);

    user.socketId = socket.id;

    users[roomId].push(user);

    updateUsersList();
  });

  socket.on('disconnect', () => {
    socket.to(roomId).emit('log', `User ${userName} disconnected`);

    users[roomId] = users[roomId].filter((usr: { socketId: string | number }) => usr.socketId !== socket.id);
    
    updateUsersList();
  });
};