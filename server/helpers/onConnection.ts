/* eslint-disable @typescript-eslint/no-explicit-any */
import { messageHandlers } from "./message.handlers";
import { userHandlers } from "./user.handlers";


export const onConnection = (io: any, socket: any) => {
  const { roomId, userName } = socket.handshake.query;

  socket.roomId = roomId;
  socket.userName = userName;

  socket.join(roomId);

  userHandlers(io, socket);
  messageHandlers(io, socket);
}