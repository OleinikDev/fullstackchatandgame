import { Schema, model } from 'mongoose';

const messagesSchema = new Schema(
  {
    massageId: {
      type: String,
      required: true,
      unique: true
    },
    messageType: {
      type: String,
      required: true
    },
    textOrPathToFile: {
      type: String,
      requred: true
    },
    roomId: {
      type: String,
      required: true
    },
    userName: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

export default model('Message', messagesSchema);