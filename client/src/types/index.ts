export type Event = {
  e: React.FormEvent<HTMLInputElement>;
};

export type FormEvent = {
  e: React.SyntheticEvent;
};
