export const storage = {
  get: (key: string) => (localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null),
  set: (key: string, value: unknown) => localStorage.setItem(key, JSON.stringify(value))
};
