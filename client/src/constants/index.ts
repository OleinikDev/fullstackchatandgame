export const USER_KEY: string = import.meta.env.VITE_USER_KEY as string;
export const SERVER_URI: string = import.meta.env.VITE_SERVER_URI as string;
