import { USER_KEY } from '@/constants';
import { storage } from '@/utils/storage';
import { nanoid } from 'nanoid';
import { useEffect, useState } from 'react';

const NameInput = () => {
  const [formData, setFormData] = useState({
    userName: '',
    roomId: 'main_room'
  });

  const [submitDisabled, setSubmitDisabled] = useState(true);

  useEffect(() => {
    const isSomeFielsEmpty = Object.values(formData).some(val => !val.trim());
    setSubmitDisabled(isSomeFielsEmpty);
  }, [formData]);

  const onChange = ({ target: { name, value } }) => {
    setFormData({ ...formData, [name]: value });
  };

  const onSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    if (submitDisabled) return;

    const userId = nanoid();

    storage.set(USER_KEY, {
      userId,
      userName: formData.userName,
      roomId: formData.roomId
    });

    window.location.reload();
  };

  return (
    <div className="container name-input">
      <h2>Welcome</h2>
      <form onSubmit={onSubmit} className="form name-room">
        <div>
          <label htmlFor="userName">Enter your name</label>
          <input
            type="text"
            id="userName"
            name="userName"
            minLength={2}
            required
            value={formData.userName}
            onChange={onChange}
          />
        </div>
        <div className="visually-hidden">
          <label htmlFor="roomId">Enter rrom ID</label>
          <input
            type="text"
            id="roomId"
            name="roomId"
            minLength={4}
            required
            value={formData.roomId}
            onChange={onChange}
          />
        </div>
        <button className="btn chat" disabled={submitDisabled}>
          Chat!
        </button>
      </form>
    </div>
  );
};

export default NameInput;
