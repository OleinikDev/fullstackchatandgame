import NameInput from '@/components/NameInput';
import Room from '@/components/Room';
import { USER_KEY } from '@/constants';
import { storage } from '@/utils/storage';

const Home = () => {
  const user = storage.get(USER_KEY);

  console.log('user', user);
  console.log('data', JSON.parse(localStorage.getItem(USER_KEY)));

  return user ? <Room /> : <NameInput />;
};

export default Home;
